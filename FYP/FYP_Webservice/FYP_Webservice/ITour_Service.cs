﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace FYP_Webservice
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        void AddTourist(string tourist_name, int tourist_age, string tourist_email, int tourist_mobile);

        [OperationContract]
        void DeleteTourist(int tourist_age);

        [OperationContract]
        List<TOURIST> ViewTourist();


        /////////// Tour Guide////////////////

        [OperationContract]
        void AddTourGuide(string tourGuide_name, int tourGuide_likes, int tourGuide_rating, int tourGuide_age,int tourGuide_mobile, string tourGuide_email);

        [OperationContract]
        void DeleteTourGuide(int tourGuide_age);

        [OperationContract]
        List<TOURGUIDE> ViewTourGuides();


        /////////// Tour_Plans/////////////
        [OperationContract]
        void AddTourPlans(string tourPlan_name, int tourPlan_likes, string tourPlan_description, int tourGuide_id);

        [OperationContract]
        void DeleteTourPlans(int tourGuide_age);

        [OperationContract]
        List<TOURPLAN> ViewTourPlans();


        //////////Maps////////////////

        [OperationContract]
        void AddMaps(int tourPlan_id, string map_name);

        [OperationContract]
        void DeleteMaps(int map_id);

        [OperationContract]
        List<MAPS> ViewMaps();


        /////////////////Marker/////////////

        [OperationContract]
        void AddMarker(string name, string address,string latitude,string longitude,int map_id);

        [OperationContract]
        void DeleteMarker(int marker_id);

        [OperationContract]
        List<MARKER> ViewMarker();
       
        //////// Subscription////////////

        [OperationContract]
        void AddSubscription(int subscription_id, int tourguide_id, int tourist_id);

        [OperationContract]
        void DeleteSubscription(int subscription_id);

        [OperationContract]
        List<SUBSCRIPTION> ViewSubscription();


        //////////// Albums//////////

        [OperationContract]
        void AddAlbums(int plan_id, string album_name, int album_likes);

        [OperationContract]
        void DeleteAlbums(int album_id);

        [OperationContract]
        List<ALBUM> ViewAlbums();


        //////////// Pictures//////////

        [OperationContract]
        void AddPictures(int album_id, int pic_likes, string pic);

        [OperationContract]
        void DeletePictures(int pic_id);

        [OperationContract]
        List<PICTURE> ViewPictures();
       
        
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    
    [DataContract]
    public class TOURIST
    {
        int touristID;
        string touristName;
        int touristAge;
        string touristEmail;
        int touristMobile;

        [DataMember]
        public int Tourist_ID
        {
            get { return touristID; }
            set { touristID = value; }
        }

         [DataMember]
        public string Tourist_Name
        {
            get { return touristName; }
            set { touristName = value; }
        }

         [DataMember]
        public int Tourist_Age
        {
            get { return touristAge; }
            set { touristAge = value; }
        }

         [DataMember]
        public string Tourist_Email
        {
            get { return touristEmail; }
            set { touristEmail = value; }
        }

         [DataMember]
        public int Tourist_Mobile
        {
            get { return touristMobile; }
            set { touristMobile = value; }
        }

    }


    ////////// TOUR GUIDE/////////////

    [DataContract]
    public class TOURGUIDE
    {
        int tourGuideID;
        string tourGuideName;
        int tourGuidelikes;
        int tourGuideRating;
        int tourGuideAge;
        int tourGuideMobile;
        string tourGuideEmail;

        [DataMember]
        public int TourGuide_ID
        {
            get { return tourGuideID; }
            set { tourGuideID = value; }
        }

        [DataMember]
        public string TourGuide_Name
        {
            get { return tourGuideName; }
            set { tourGuideName = value; }
        }

        [DataMember]
        public int TourGuide_Likes
        {
            get { return tourGuidelikes; }
            set { tourGuidelikes = value; }
        }

        [DataMember]
        public int TourGuide_Rating
        {
            get { return tourGuideRating; }
            set { tourGuideRating = value; }
        }

        [DataMember]
        public int TourGuide_Age
        {
            get { return tourGuideAge; }
            set { tourGuideAge = value; }
        }

        public int TourGuide_Mobile
        {
            get { return tourGuideMobile; }
            set { tourGuideMobile = value; }
        }

        public string TourGuide_Email
        {
            get { return tourGuideEmail; }
            set { tourGuideEmail = value; }
        }

    }


    [DataContract]
    public class TOURPLAN
    {
        int tourPlanID;
        string tourPlanName;
        int tourPlanLikes;
        string tourPlanDescription;
        int tourGuideID;

        [DataMember]
        public int TourPlan_ID
        {
            get { return tourPlanID; }
            set { tourPlanID = value; }
        }

        [DataMember]
        public string TourPlan_Name
        {
            get { return tourPlanName; }
            set { tourPlanName = value; }
        }

        [DataMember]
        public int TourPlan_Likes
        {
            get { return tourPlanLikes; }
            set { tourPlanLikes = value; }
        }

        [DataMember]
        public string TourPlan_Description
        {
            get { return tourPlanDescription; }
            set { tourPlanDescription = value; }
        }

        public int TourGuide_ID
        {
            get { return tourGuideID; }
            set { tourGuideID = value; }
        }

    }

    public class MAPS
    {
        int tourplanID;
        int mapID;
        string mapName;
        
        [DataMember]
        public int TourPlan_ID
        {
            get { return tourplanID; }
            set { tourplanID = value; }
        }

        [DataMember]
        public int map_id
        {
            get { return mapID; }
            set { mapID = value; }
        }

        [DataMember]
        public string map_Name
        {
            get { return mapName; }
            set { mapName = value; }
        }


    }


    //////// MARKER///////////////

    public class MARKER
    {
        string mName;
        int markerID;
        string Address;
        string Lat;
        string Long;
        int mapID;


        [DataMember]
        public string marker_Name
        {
            get { return mName; }
            set { mName = value; }
        }

        [DataMember]
        public int marker_ID
        {
            get { return markerID; }
            set { markerID = value; }
        }

        [DataMember]
        public string marker_Address
        {
            get { return Address; }
            set { Address = value; }
        }

        public string marker_Lat
        {
            get { return Lat; }
            set { Lat = value; }
        }


        public string marker_Long
        {
            get { return Long; }
            set { Long = value; }
        }

        public int map_ID
        {
            get { return mapID; }
            set { mapID = value; }
        }




    }


    //////////Subscription/////////

    public class SUBSCRIPTION
    {
        int subscriptionID;
        int tourGuideID;
        int touristID;

        [DataMember]
        public int Subscription_ID
        {
            get { return subscriptionID; }
            set {subscriptionID = value; }
        }

        [DataMember]
        public int TourGuide_ID
        {
            get { return tourGuideID; }
            set { tourGuideID = value; }
        }

        [DataMember]
        public int Tourist_ID
        {
            get { return touristID; }
            set { touristID = value; }
        }


    }


    public class ALBUM
    {
        int albumID;
        int planID;
        string albumName;
        int albumLikes;

        [DataMember]
        public int album_ID
        {
            get { return albumID; }
            set { albumID = value; }
        }

        [DataMember]
        public int plan_ID
        {
            get { return planID; }
            set { planID = value; }
        }


        [DataMember]
        public string album_name
        {
            get { return albumName; }
            set { albumName = value; }
        }

        [DataMember]
        public int album_likes
        {
            get { return albumLikes; }
            set { albumLikes = value; }
        }


    }

    ///////////////PICTURES//////////////////

    public class PICTURE
    {
        int picID;
        int albumID;
        int picLikes;
        string picture;

        [DataMember]
        public int pic_ID
        {
            get { return picID; }
            set { picID = value; }
        }

        [DataMember]
        public int album_ID
        {
            get { return albumID; }
            set { albumID = value; }
        }


        [DataMember]
        public int pic_likes
        {
            get { return picLikes; }
            set { picLikes = value; }
        }

        [DataMember]
        public string Picture
        {
            get { return picture; }
            set { picture = value; }
        }


    }

   
}
