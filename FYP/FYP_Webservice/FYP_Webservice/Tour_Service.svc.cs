﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace FYP_Webservice
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        //**TRANSLATIONS

        public void AddTourist(string tourist_name, int tourist_age, string tourist_email, int tourist_mobile)
        {
            DataClasses1DataContext cntx = new DataClasses1DataContext();
            Tourist touristObject = new Tourist();
           // touristObject.TO_id = tourist_id;
            touristObject.TO_name = tourist_name;
            touristObject.TO_age = tourist_age;
            touristObject.TO_email = tourist_email;
            touristObject.TO_mobile = tourist_mobile;
            cntx.Tourists.InsertOnSubmit(touristObject);
            cntx.SubmitChanges();

        }
        public void DeleteTourist(int tourist_age)
        {
            DataClasses1DataContext Context = new DataClasses1DataContext();
            var delete_tourist = Context.Tourists.Single(item => item.TO_age == (tourist_age));
            Context.Tourists.DeleteOnSubmit(delete_tourist);
            Context.SubmitChanges();

        }

        public List<TOURIST> ViewTourist()
        {

            DataClasses1DataContext Context = new DataClasses1DataContext();
            var query = from a in Context.Tourists
                        select new TOURIST()
                        {
                            Tourist_ID = a.TO_id,
                            Tourist_Name = a.TO_name,
                            Tourist_Age = a.TO_age,
                            Tourist_Email = a.TO_email,
                            Tourist_Mobile = a.TO_mobile,
                        };

            List<TOURIST> lstTourist = query.ToList<TOURIST>();
            return lstTourist;

        }


        ///////////////// Tour Guide//////////////


        public void AddTourGuide(string tourGuide_name, int tourGuide_likes, int tourGuide_rating, int tourGuide_age, int tourGuide_mobile, string tourGuide_email)
        {

            DataClasses1DataContext cntx = new DataClasses1DataContext();
            Tour_Guide tourGuideObject = new Tour_Guide();
            //tourGuideObject.TG_id = tourGuide_id;
            tourGuideObject.TG_name = tourGuide_name;
            tourGuideObject.TG_likes = tourGuide_likes;
            tourGuideObject.TG_rating = tourGuide_rating;
            tourGuideObject.TG_age = tourGuide_age;
            tourGuideObject.TG_mobile = tourGuide_mobile;
            tourGuideObject.TG_email = tourGuide_email;
            cntx.Tour_Guides.InsertOnSubmit(tourGuideObject);
            cntx.SubmitChanges();

        
        }

        public void DeleteTourGuide(int tourGuide_age)
        {
            DataClasses1DataContext Context = new DataClasses1DataContext();
            var delete_tourGuide = Context.Tour_Guides.Single(item => item.TG_age == (tourGuide_age));
            Context.Tour_Guides.DeleteOnSubmit(delete_tourGuide);
            Context.SubmitChanges();

        }


        public List<TOURGUIDE> ViewTourGuides()
        {

            DataClasses1DataContext Context = new DataClasses1DataContext();
            var query = from a in Context.Tour_Guides
                        select new TOURGUIDE()
                        {
                            TourGuide_ID = a.TG_id,
                            TourGuide_Name = a.TG_name,
                            TourGuide_Likes = a.TG_likes,
                            TourGuide_Rating = a.TG_rating,
                            TourGuide_Age = a.TG_age,
                            TourGuide_Mobile = a.TG_mobile,
                            TourGuide_Email = a.TG_email,


                        };

            List<TOURGUIDE> lstTourGuides = query.ToList<TOURGUIDE>();
            return lstTourGuides;

        }



        ////////////////////ADD Tour Plans////////////////////

        public void AddTourPlans(string tourPlan_name, int tourPlan_likes, string tourPlan_description, int tourGuide_id)
        {
            DataClasses1DataContext cntx = new DataClasses1DataContext();
            Tour_Plan tourPlanObject = new Tour_Plan();
           // tourPlanObject.P_id=tourPlan_id;
            tourPlanObject.P_name = tourPlan_name;
            tourPlanObject.P_likes = tourPlan_likes;
            tourPlanObject.P_description = tourPlan_description;
            tourPlanObject.TG_id = tourGuide_id;
            cntx.Tour_Plans.InsertOnSubmit(tourPlanObject);
            cntx.SubmitChanges();

        
        }

        public void DeleteTourPlans(int tourPlan_id)
        {
            DataClasses1DataContext Context = new DataClasses1DataContext();
            var delete_tourPlan = Context.Tour_Plans.Single(item => item.P_id == (tourPlan_id));
            Context.Tour_Plans.DeleteOnSubmit(delete_tourPlan);
            Context.SubmitChanges();

        }

        public List<TOURPLAN> ViewTourPlans()
        {

            DataClasses1DataContext Context = new DataClasses1DataContext();
            var query = from a in Context.Tour_Plans
                        select new TOURPLAN()
                        {
                            TourPlan_ID = a.P_id,
                            TourPlan_Name = a.P_name,
                            TourPlan_Likes = a.P_likes,
                            TourPlan_Description = a.P_description,
                            TourGuide_ID = a.TG_id,

                            
                        };

            List<TOURPLAN> lstTourPlans = query.ToList<TOURPLAN>();
            return lstTourPlans;

        }



        ///////////////////MAPS//////////////////////////////

       public void AddMaps(int tourPlan_id, string map_name)
        {
            DataClasses1DataContext cntx = new DataClasses1DataContext();
            Map MapsObject = new Map();
            MapsObject.P_id = tourPlan_id;
            //MapsObject.M_id = map_id;
            MapsObject.M_name=map_name;
            cntx.Maps.InsertOnSubmit(MapsObject);
            cntx.SubmitChanges();
        }

       public void DeleteMaps(int map_id)
       {
           DataClasses1DataContext Context = new DataClasses1DataContext();
           var delete_map = Context.Maps.Single(item => item.M_id == (map_id));
           Context.Maps.DeleteOnSubmit(delete_map);
           Context.SubmitChanges();

       }


       public List<MAPS> ViewMaps()
       {

           DataClasses1DataContext Context = new DataClasses1DataContext();
           var query = from a in Context.Maps
                       select new MAPS()
                       {
                           TourPlan_ID = a.P_id,
                           map_id = a.M_id,
                           map_Name = a.M_name,
                           

                       };

           List<MAPS> lstMaps = query.ToList<MAPS>();
           return lstMaps;

       }

        ////////////////////Marker//////////////


      public void AddMarker(string name, string address, string latitude, string longitude, int map_id)
       {
           DataClasses1DataContext cntx = new DataClasses1DataContext();
           Marker MarkerObject = new Marker();
           MarkerObject.Name = name;
           //MarkerObject.Mar_ID = marker_id;
           MarkerObject.Address = address;
           MarkerObject.Lat = latitude;
           MarkerObject.Lng = longitude;
           MarkerObject.M_id = map_id;
           cntx.Markers.InsertOnSubmit(MarkerObject);
           cntx.SubmitChanges();

       }

      public void DeleteMarker(int marker_id)
      {
          DataClasses1DataContext Context = new DataClasses1DataContext();
          var delete_marker = Context.Markers.Single(item => item.Mar_ID == (marker_id));
          Context.Markers.DeleteOnSubmit(delete_marker);
          Context.SubmitChanges();

      }

      public List<MARKER> ViewMarker()
      {

          DataClasses1DataContext Context = new DataClasses1DataContext();
          var query = from a in Context.Markers
                      select new MARKER()
                      {
                          marker_Name = a.Name,
                          marker_ID = a.Mar_ID,
                          marker_Address = a.Address,
                          marker_Lat = a.Lat,
                          marker_Long = a.Lng,
                          map_ID = a.M_id,
                          
                      };

          List<MARKER> lstMarkers = query.ToList<MARKER>();
          return lstMarkers;

      }


        /////////// Subscription////////////

      public void AddSubscription(int subscription_id, int tourguide_id, int tourist_id)
      {
          DataClasses1DataContext cntx = new DataClasses1DataContext();
          Subscription SubscriptionObject = new Subscription();
          SubscriptionObject.SUB_id = subscription_id;
          SubscriptionObject.TG_id = tourguide_id;
          SubscriptionObject.TO_id = tourist_id;
          cntx.Subscriptions.InsertOnSubmit(SubscriptionObject);
          cntx.SubmitChanges();

      }

      public void DeleteSubscription(int subscription_id)
      {
          DataClasses1DataContext Context = new DataClasses1DataContext();
          var delete_subs = Context.Subscriptions.Single(item => item.SUB_id == (subscription_id));
          Context.Subscriptions.DeleteOnSubmit(delete_subs);
          Context.SubmitChanges();

      }


      public List<SUBSCRIPTION> ViewSubscription()
      {

          DataClasses1DataContext Context = new DataClasses1DataContext();
          var query = from a in Context.Subscriptions
                      select new SUBSCRIPTION()
                      {
                          Subscription_ID = a.SUB_id,
                          TourGuide_ID = a.TG_id,
                          Tourist_ID = a.TO_id,
                          
                      };

          List<SUBSCRIPTION> lstSubs = query.ToList<SUBSCRIPTION>();
          return lstSubs;

      }


        ///////////////Album////////////////

      public void AddAlbums(int plan_id, string album_name, int album_likes)
      {
          DataClasses1DataContext cntx = new DataClasses1DataContext();
          Album AlbumObject = new Album();
         // AlbumObject.Album_id = album_id;
          AlbumObject.P_id = plan_id;
          AlbumObject.Album_name = album_name;
          AlbumObject.Album_likes = album_likes;
          cntx.Albums.InsertOnSubmit(AlbumObject);
          cntx.SubmitChanges();

      }

      public void DeleteAlbums(int album_id)
      {
          DataClasses1DataContext Context = new DataClasses1DataContext();
          var delete_albums = Context.Albums.Single(item => item.Album_id == (album_id));
          Context.Albums.DeleteOnSubmit(delete_albums);
          Context.SubmitChanges();

      }


      public List<ALBUM> ViewAlbums()
      {

          DataClasses1DataContext Context = new DataClasses1DataContext();
          var query = from a in Context.Albums
                      select new ALBUM()
                      {
                          album_ID = a.Album_id,
                          plan_ID = a.P_id,
                          album_name = a.Album_name,
                          album_likes = a.Album_likes,


                      };

          List<ALBUM> lstAlbum = query.ToList<ALBUM>();
          return lstAlbum;

      }


        ////////////////// PICTURES//////////////////
        
      public void AddPictures(int album_id, int pic_likes, string pic)
      {
          DataClasses1DataContext cntx = new DataClasses1DataContext();
          Picture PictureObject = new Picture();
          //PictureObject.Pic_id = pic_id;
          PictureObject.Album_id = album_id;
          PictureObject.Pic_likes = pic_likes;
          PictureObject.Pic = pic;
          cntx.Pictures.InsertOnSubmit(PictureObject);
          cntx.SubmitChanges();

      }

      public void DeletePictures(int pic_id)
      {
          DataClasses1DataContext Context = new DataClasses1DataContext();
          var delete_pics = Context.Pictures.Single(item => item.Pic_id == (pic_id));
          Context.Pictures.DeleteOnSubmit(delete_pics);
          Context.SubmitChanges();

      }

      public List<PICTURE> ViewPictures()
      {

          DataClasses1DataContext Context = new DataClasses1DataContext();
          var query = from a in Context.Pictures
                      select new PICTURE()
                      {
                          pic_ID = a.Pic_id,
                          album_ID = a.Album_id,
                          pic_likes = a.Pic_likes,
                          Picture = a.Pic,


                      };

          List<PICTURE> lstPic = query.ToList<PICTURE>();
          return lstPic;

      }


    }
}
