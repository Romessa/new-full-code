﻿using ServiceReference1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AddTourGuide : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
       // int tourGuide_id = TextBox1.Text;
        string tourGuide_name = TextBox1.Text;
        int tourGuide_likes =   int.Parse(TextBox7.Text);
        int tourGuide_rating =  int.Parse(TextBox8.Text);
        int tourGuide_age =     int.Parse(TextBox9.Text);
        int tourGuide_mobile =  int.Parse(TextBox10.Text);
        string tourGuide_email =TextBox11.Text;
        Service1Client client = new Service1Client();
        client.AddTourGuide(tourGuide_name, tourGuide_likes, tourGuide_rating, tourGuide_age, tourGuide_mobile, tourGuide_email);  
    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        
        // int tourGuide_id = TextBox1.Text;
        string tourGuide_name = TextBox1.Text;
        int tourGuide_likes = int.Parse(TextBox7.Text);
        int tourGuide_rating = int.Parse(TextBox8.Text);
        int tourGuide_age = int.Parse(TextBox9.Text);
        int tourGuide_mobile = int.Parse(TextBox10.Text);
        string tourGuide_email = TextBox11.Text;
        Service1Client client = new Service1Client();
        client.AddTourGuide(tourGuide_name, tourGuide_likes, tourGuide_rating, tourGuide_age, tourGuide_mobile, tourGuide_email);
        Response.Redirect("GuideMenu.aspx");
    
    }
}