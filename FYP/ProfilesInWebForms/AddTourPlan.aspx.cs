﻿using ServiceReference1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AddTourPlan : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    // 
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {

        string tourGuide_name = TextBox1.Text;
        int tourGuide_likes = int.Parse(TextBox7.Text);
        string tourGuide_description = TextBox8.Text;
        int TG_id = int.Parse(TextBox9.Text);

        Service1Client client = new Service1Client();
        client.AddTourPlans(tourGuide_name, tourGuide_likes, tourGuide_description, TG_id);
    }
}