﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WELCOME.aspx.cs" Inherits="WELCOME" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <html dir="ltr" lang="en-US" class="no-js">
<head>
	<!-- ==============================================
	Title and Meta Tags
	=============================================== -->
	<meta charset="utf-8">
	<title>TripCo</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="" />
	
	<!-- ==============================================
	Mobile
	=============================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    
	<!-- ==============================================
	Styles
	=============================================== -->
	<link rel="stylesheet" type="text/css" media="screen" href="assets/css/reset.min.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="assets/css/style.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="assets/css/swipebox.min.css" />
	
	<!--[if lt IE 9]>
	    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<!--[if lt IE 10]>
		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/ie.css" />
	<![endif]-->
	
	<!-- ==============================================
	Fonts
	=============================================== -->
	<link href='../../fonts.googleapis.com/css.css' rel='stylesheet' type='text/css'>
	
	<!-- ==============================================
	Font Icons
	=============================================== -->
	<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css" />
	
	<!-- ==============================================
	Modernizr
	=============================================== -->
	<script src="assets/scripts/modernizr.min.js" type="text/javascript"></script>
	
	<!-- ==============================================
	Favicons
	=============================================== -->
	<link rel="shortcut icon" href="http://themes.wearesupa.com/static/tipi/assets/images/favicon.ico">
	<link rel="apple-touch-icon" href="http://themes.wearesupa.com/static/tipi/images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="http://themes.wearesupa.com/static/tipi/images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="http://themes.wearesupa.com/static/tipi/images/apple-touch-icon-114x114.png">
	
	<script>
	    (function (i, s, o, g, r, a, m) {
	        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
	            (i[r].q = i[r].q || []).push(arguments)
	        }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
	    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

	    ga('create', 'UA-45247204-1', 'wearesupa.com');
	    ga('send', 'pageview');
	</script>
</head>

<body class="home">
	<div id="container">
		
		<!-- ==============================================
		Preloader
		=============================================== -->
		<div id="preloader">
		    <div id="loading-animation"></div>
		</div>
		<!--/ Preloader -->
		
		<!-- ==============================================
		Menu Overlay
		=============================================== -->
		<div id="navtrigger"><span class="top"></span><span class="middle"></span><span class="middlecopy"></span><span class="bottom"></span></div>
		
		<div id="menuoverlay">
			<!-- Main Menu -->
				<div class="col span1of4">
				    <h3>REGISTER</h3>
				    <nav id="main">
				        <ul>
				            <li><a href="AddTourist.aspx"> Tourist</a></li>
				            <li><a href="AddTourGuide.aspx">TourGuide</a></li>
				           
				        
                               </ul>
				    </nav>
				</div>
				<!-- End Main Menu -->
				
				<!-- Recent Posts -->
    			<div class="col span1of4">
    				<h3>Recents</h3>
    				<ol>
    					<li><a href="ViewAllTourGuides.aspx">Tour Guides</a></li>
    					<li><a href="ViewAllTourPlans.aspx">Tour Plans</a></li>
    					
    				</ol>
    			</div>
			    <!--/ Recent Posts -->
		    
		    <!-- Archives 		
			<div class="col span1of4">
				<h3>Archives</h3>
				<ol>
					<li><a href="tipi.html">March, 2014</a></li>
					<li><a href="tipi.html">February, 2014</a></li>
					<li><a href="tipi.html">January, 2014</a></li>
					<li><a href="tipi.html">December, 2013</a></li>
				</ol>
			</div>
                -->
		    <!--/ Archives -->
		    
		    <!-- Categories -->			
			<div class="col span1of4">
				<h3>Categories</h3>
				<ul>
					
					<li><a href="ViewAllPictures">Photos</a></li>
                    <li><a href="ViewAllMaps">Maps</a></li>
				</ul>
			</div>
			<!--/ Categories -->
			
			<!-- Contact -->
			<div  id="contact" class="row">
    			<div class="col span3of3">
    				<i class="fa fa-envelope-o"></i>
    				<a href="mailto:hello@wearesupa.com" title="Email me">Tripco@facebook.com</a> | 0321-5102612
    			</div>
			</div>
			<!--/ Contact -->
		</div>
		<!--/ Menu Overlay -->
		
		<!-- ==============================================
		Cover
		=============================================== -->
        <div id="cover">
            <div class="inner">
                <div class="col span3of3 parallax">
                    <div id="logo"><img src="http://themes.wearesupa.com/static/tipi/assets/images/tipi-logo.svg" alt="TripCo" title="TripCo" height="150"></div>

                    <h1 style="font-family:'Monotype Corsiva';font:50%">TRIPCO</h1></br>
                    <h1><span>Your</span> Ultimate Travel Companion</h1>

                    <a href="#contentwrap" class="btn down">Tour Guides</a>
                </div>
            </div>
        </div>
		<!--/ Cover -->
		
		<div id="home"></div>
		
		<!-- ==============================================
	    Main Content
	    =============================================== -->
        <div id="contentwrap">

            <!-- Blog Panel -->
            <div id="blog" class="row">
                <div class="inner">
                    <div class="col span3of3">
                        <h2 class="title">Tour Guides</h2>

                        <p>A list of tour guides. Each guide has his own personal profile in our application. He can upload content related to tourism places also tour plans.</p>
                    </div>
                </div>

                <div class="inner">
                    <ul class="articlelist">
                        <li class="item">
                            <section class="view">


                                <h3><a href="http://themes.wearesupa.com/static/tipi/article.html" title="Read">John Wright</a></h3>
                                <a href="FbJohn.aspx"><img src="assets/images/John-wright.jpg" alt="assets/images/John-wright.jpg" /></a>

                                <footer>
                                    <section class="clearfix">
                                        <div class="left"><i class="fa fa-comments"></i> 8.1</div> <div class="right"><i class="fa fa-pencil"></i> January 10, 2014</div>
                                    </section>
                                </footer>
                            </section>
                        </li>

                        <li class="item">
                            <section class="view">
                                <h3><a href="http://themes.wearesupa.com/static/tipi/article.html" title="Read">Diego Torres</a></h3>
                                <a href="FbDiego.aspx"><img src="assets/images/Diego-Torres.jpg" alt="assets/images/Diego-Torres.jpg" /></a>

                                <footer>
                                    <section class="clearfix">
                                        <div class="left"><i class="fa fa-comments"></i> 5.5</div> <div class="right"><i class="fa fa-pencil"></i> January 10, 2014</div>
                                    </section>
                                </footer>
                            </section>
                        </li>

                        <li class="item">
                            <section class="view">

                                <h3><a href="http://themes.wearesupa.com/static/tipi/article.html" title="Read">Tajamul Hussain</a></h3>
                                <a href="http://themes.wearesupa.com/static/tipi/article.html"><img src="assets/images/tajamul.jpg" alt="assets/images/tajamul.jpg" /></a>


                                <footer>
                                    <section class="clearfix">
                                        <div class="left"><i class="fa fa-comments"></i> 7.7</div> <div class="right"><i class="fa fa-pencil"></i> January 10, 2014</div>
                                    </section>
                                </footer>
                            </section>
                        </li>

                        <li class="item">
                            <section class="view">

                                <h3><a href="http://themes.wearesupa.com/static/tipi/article.html" title="Read">Kevin Ainsley</a></h3>
                                <a href="http://themes.wearesupa.com/static/tipi/article.html"><img src="assets/images/kevin-ainsley.JPG" alt="assets/images/ kevin-ainsley.JPG" /></a>


                                <footer>
                                    <section class="clearfix">
                                        <div class="left"><i class="fa fa-comments"></i> 4.6</div> <div class="right"><i class="fa fa-pencil"></i> January 10, 2014</div>
                                    </section>
                                </footer>
                            </section>
                        </li>

                        <li class="item">
                            <section class="view">

                                <h3><a href="http://themes.wearesupa.com/static/tipi/article.html" title="Read">Tim Janksons</a></h3>
                                <a href="http://themes.wearesupa.com/static/tipi/article.html"><img src="assets/images/Tim-Janksons.jpg" alt="assets/images/Tim-Janksons.jpg" /></a>
                                <footer>
                                    <section class="clearfix">
                                        <div class="left"><i class="fa fa-comments"></i> 7.8</div> <div class="right"><i class="fa fa-pencil"></i> January 10, 2014</div>
                                    </section>
                                </footer>
                            </section>
                        </li>

                        <li class="item">
                            <section class="view">
                                <h3><a href="http://themes.wearesupa.com/static/tipi/article.html" title="Read">Shahid Yusuf</a></h3>
                                <a href="http://themes.wearesupa.com/static/tipi/article.html"><img src="assets/images/shahid-yusuf.jpg" alt="assets/images/shahid-yusuf.jpg" /></a>
                                <footer>
                                    <section class="clearfix">
                                        <div class="left"><i class="fa fa-comments"></i> 6.9</div> <div class="right"><i class="fa fa-pencil"></i> January 10, 2014</div>
                                    </section>
                                </footer>
                            </section>
                        </li>
                    </ul>
                    <a href="http://themes.wearesupa.com/static/tipi/blog.html" class="btn clear">More</a>
                </div>
            </div>
            <!--/ Blog Panel -->
            <!--/ About Panel -->
            <!-- Stats Panel -->
            <!--/ Stats Panel -->
            <!-- Footer -->
            <div id="footer" class="row">
                <div class="inner clearfix">
                    <div class="textblock">
                        <!-- Social Links -->
                        <div class="sociallinks">
                            <ul>
                                <li><a href="http://twitter.com" title="Twitter" class="btn btndark">Twitter</a></li>
                                <li><a href="http://facebook.com" title="Facebook" class="btn btndark">Facebook</a></li>
                                <li><a href="http://instagram.com" title="Instagram" class="btn btndark">Instagram</a></li>
                                <li><a href="http://foursquare.com" title="Foursquare" class="btn btndark">Foursquare</a></li>
                                <li><a href="http://youtube.com" title="Youtube" class="btn btndark">Youtube</a></li>
                            </ul>
                        </div>
                        <!--/ Social Links -->

                        <span class="copyright">Copyright &copy; 2014 - Tipi</span>
                    </div>
                </div>
            </div>
            <!--/ Footer -->
        </div>
		<!--/ Main Content -->
		
		<a href="tipi.html#container" class="up scrollup">up</a>
	</div>

	<!-- Page border -->
	<b id="tborder"></b><b id="rborder"></b><b id="bborder"></b><b id="lborder"></b>
    
	<!-- ==============================================
	Scripts
	=============================================== -->
	<script src="assets/scripts/jquery.min.js" type="text/javascript"></script>
	<script src="assets/scripts/scripts.js" type="text/javascript"></script>
	<script src="assets/scripts/jquery.global.js" type="text/javascript"></script>
	<!--/ Scripts -->
	
</body>
</html>

    
    </div>
    </form>
</body>
</html>
